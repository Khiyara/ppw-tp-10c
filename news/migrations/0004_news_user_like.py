# Generated by Django 2.0.6 on 2018-10-17 15:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0003_auto_20181017_2230'),
        ('news', '0003_auto_20181017_1956'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='user_like',
            field=models.ManyToManyField(to='dashboard.User'),
        ),
    ]
