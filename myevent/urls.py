from django.urls import path
from . import views
from .views import myevent

urlpatterns = [
    path('', views.myevent, name="myevent"),
]
