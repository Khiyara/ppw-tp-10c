from django.shortcuts import render, redirect
from events.models import Events
from dashboard.models import User


def id(request, events_id):
    try:
        events = Events.objects.get(pk=events_id)
    except:
        raise Http404('Events doesnt exist')
    return redirect('/events/' + str(events.id))
# Create your views here.
def myevent(request):
    list_event = Events.objects.all()
    user_now = User.objects.get(email = request.user.email)
    list_event_new = []
    for event in list_event:
        for user in event.participant.all():
            if user == user_now:
                list_event_new.append(event)
    
    # events = Events.objects.filter(participant=request.user)
    return render(request, 'myevent.html', {'list_event_new':list_event_new})