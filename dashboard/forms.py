from django import forms
from .models import User


class SignUp(forms.ModelForm):
    name = forms.CharField(max_length=30, required=True)
    username = forms.CharField(max_length=30, required=True)
    email = forms.EmailField(widget=forms.EmailInput(attrs={'type':'email'}),max_length=254,required = True)
    password = forms.CharField(widget=forms.PasswordInput(attrs={'type':'password'}),max_length=8, required = True)
    birthday = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}), required=True)
    live = forms.CharField(max_length=40, required=True)
    class Meta:
        model = User
        fields = ('username', 'name', 'email', 'password','birthday', 'live', )
