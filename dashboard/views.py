from django.shortcuts import render, redirect
from .models import User
from django.http import Http404, HttpResponseNotFound
from news.models import News
from events.models import Events
from .forms import SignUp
# Create your views here.
def dashboard(request):
    list_news = News.objects.all()
    list_events = Events.objects.all()
    if not request.user.is_authenticated:
        request.session.flush()
        return render(request, 'homepage.html', {'news':list_news,'events':list_events})
    username = request.user.username
    email = request.user.email
    nama = request.user.first_name
    user = User.objects.filter(username=username, email=email, name=nama)
    if user:
        return render(request, 'homepage.html', {'news':list_news,'events':list_events})
    user = User(username=username, email=email, name=nama)
    user.save()
    return render(request, 'homepage.html', {'news':list_news,'events':list_events})

def register(request):
    if (request.method == 'POST'):
        form = SignUp(request.POST or None)
        if form.is_valid():
            name = request.POST['name']
            username = request.POST['username']
            email = request.POST['email']
            password = request.POST['password']
            birthday = request.POST['birthday']
            live = request.POST['live']
            list_user = User.objects.filter(email=email)
            if (list_user != None):
                post = User(name=name, username=username,
                          email=email, password=password, birthday=birthday,
                            live=live)
                post.save()
                return redirect('/')
            else:
                form = SignUp()
            return render(request, 'register.html', {'form':form})
    else:
        form = SignUp()
    return render(request, 'register.html', {'form':form})
