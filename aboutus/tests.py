from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import about, testimoni
from .models import Testimoni
# Create your tests here.

class AboutUsTest(TestCase):
    def test_aboutus_url_is_exist(self):
        response = Client().get('/aboutus/')
        self.assertEqual(response.status_code, 200)
        
    def test_aboutus_using_about_func(self):
        function = resolve('/aboutus/')
        self.assertEqual(function.func, about)

    def test_testimoni_url_is_exitst(self):
        response = Client().get('/aboutus/testimoni/')
        self.assertEqual(response.status_code, 200)

    def test_testimoni_create_model(self):
        testimoni = Testimoni.objects.create(username="User", email="Pepega@gmail.com", text="test")
        count_variables = Testimoni.objects.all().count()
        self.assertEqual(count_variables , 1)

    def test_testimoni_using_function(self):
        function = resolve('/aboutus/testimoni/')
        self.assertEqual(function.func, testimoni)

    def test_testimoni_using_template(self):
        response = Client().get('/aboutus/testimoni/')
        self.assertTemplateUsed(response, 'testimoni.html')

    def test_testimoni_has_button_submit(self):
        response = Client().get('/aboutus/testimoni/')
        self.assertContains(response, 'Submit your testimoni')

    def test_testimoni_json_user(self):
        response = Client().get('/aboutus/list_user/')
        self.assertEqual(response.status_code, 200)
