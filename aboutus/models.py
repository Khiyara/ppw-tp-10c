from django.db import models

# Create your models here.
class Testimoni(models.Model):
    username = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    text = models.CharField(max_length=300)
