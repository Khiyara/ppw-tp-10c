from django.shortcuts import render
from .models import Testimoni
from dashboard.models import User
from aboutus.forms import TestimoniForms
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.http import HttpResponseRedirect
import json
# Create your views here.
def about(request):
    return render(request, 'aboutus.html')


def testimoni(request):
    if not request.user.is_authenticated:
        return render(request, 'testimoni.html')
    if request.method == "POST":
        form = TestimoniForms(request.POST or None)
        if form.is_valid():
            nama = request.user.username
            email = request.user.email
            text = form.cleaned_data['text'].replace("<","").replace(">","")
            if(len(text) < 1):
                testimoni = User().save() #sengaja biar error
            testimoni = Testimoni(username=nama, email=email, text=text)
            testimoni.save()
    else:
        form = TestimoniForms()
    return render(request, 'testimoni.html')

def list_user(request):
    list_testimoni = Testimoni.objects.all()
    list_testimoni_query = []
    index = 0
    for x in list_testimoni:
        list_testimoni_query.append({'nama':x.username, 'email':x.email, 'text':x.text})
        index += 1
    return HttpResponse(json.dumps({'data':list_testimoni_query,'message':'hei'}), content_type="application/json")

