from django import forms
from .models import Testimoni


class TestimoniForms(forms.ModelForm):
    text = forms.CharField(widget=forms.PasswordInput(attrs={'type':'text'}),max_length=300, required = True)
        
    class Meta:
        model = Testimoni
        fields = ('text',)
    
